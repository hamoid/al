# Artificial Life (2022)

![](images/screenshots/126395.jpg)

An art project by Abraham Pazos Solatie funded from May until October by
the grant:

[Stipendium NEUSTART KULTUR „Modul D – Digitale Vermittlungsformate“](https://www.kuenstlerbund.de/deutsch/projekte/projekte-ab-2011/2022_stipendiat_innen_runde3.html)

<table><tr>
    <td><img src="images/logos/CDR_BKM_Neustart_Kultur_Wortmarke_pos_RGB_RZ.svg" width="120" align="middle"></td>
    <td><img src="images/logos/BKM_Web_de.gif" align="middle"></td>
    <td><img src="images/logos/dkb_logo_1,9 x 6,4cm_RGB_300dpi.png" width="250" align="middle"></td>
</tr></table>

## Purpose of this repository

To track progress of a 5-month project from beginning to end.
To keep track of time and document goals and ideas.
Let others who may be interested in following this project to do so. 

## Calendar to track progress & time

|     | M        | T      | W      | T      | F      | S      | S       |
|-----|----------|--------|--------|--------|--------|--------|---------|
| May | ~~16~~   | ~~17~~ | ~~18~~ | ~~19~~ | ~~20~~ | ~~21~~ | ~~22~~  |
|     | ~~23~~   | ~~24~~ | ~~25~~ | ~~26~~ | ~~27~~ | ~~28~~ | ~~29~~  |
| Jun | ~~30~~   | ~~31~~ | ~~1~~  | ~~2~~  | ~~3~~  | ~~4~~  | ~~5~~   |
|     | ~~6~~    | ~~7~~  | ~~8~~  | ~~9~~  | ~~10~~ | ~~11~~ | ~~12~~  |
|     | ~~13~~   | ~~14~~ | ~~15~~ | ~~16~~ | ~~17~~ | ~~18~~ | ~~19~~  |
|     | ~~20~~   | ~~21~~ | ~~22~~ | ~~23~~ | ~~24~~ | ~~25~~ | ~~26~~  |
| Jul | ~~27~~   | ~~28~~ | ~~29~~ | ~~30~~ | ~~1~~  | ~~2~~  | ~~3~~   |
|     | ~~4~~    | ~~5~~  | ~~6~~  | ~~7~~  | ~~8~~  | ~~9~~  | ~~10~~  |
|     | ~~11~~   | ~~12~~ | ~~13~~ | ~~14~~ | ~~15~~ | ~~16~~ | ~~17~~  |
|     | ~~18~~   | ~~19~~ | ~~20~~ | ~~21~~ | ~~22~~ | ~~23~~ | ~~24~~  |
|     | ~~25~~   | ~~26~~ | ~~27~~ | ~~28~~ | ~~29~~ | ~~30~~ | ~~31~~  |
| Aug | ~~1~~    | ~~2~~  | ~~3~~  | ~~4~~  | ~~5~~  | ~~6~~  | ~~7~~   |
|     | ~~8~~    | ~~9~~  | ~~10~~ | ~~11~~ | ~~12~~ | ~~13~~ | ~~14~~  |
|     | ~~15~~   | ~~16~~ | ~~17~~ | ~~18~~ | ~~19~~ | ~~20~~ | ~~21~~  |
|     | ~~22~~   | ~~23~~ | ~~24~~ | ~~25~~ | ~~26~~ | ~~27~~ | ~~28~~  |
| Sep | ~~29~~   | ~~30~~ | ~~31~~ | ~~1~~  | ~~2~~  | ~~3~~  | ~~4~~   |
|     | ~~5~~    | ~~6~~  | ~~7~~  | ~~8~~  | ~~9~~  | ~~10~~ | ~~11~~  |
|     | ~~12~~   | ~~13~~ | ~~14~~ | ~~15~~ | ~~16~~ | ~~17~~ | ~~18~~  |
|     | ~~19~~   | ~~20~~ | ~~21~~ | ~~22~~ | 23     | 24     | 25      |
| Oct | 26       | 27     | 28     | 29     | 30     | 1      | 2       |
|     | 3        | 4      | 5      | 6      | 7      | 8      | 9       |
|     | 10       | 11     | 12     | 13     | 14     |        |         |  



## Day 1

Friday last week I received a call about this grant. A previous e-mail informed
me that the grant had been rejected. It seems like after some evaluation several
grant winners turned out not to be valid recipients, so in that call I was
notified that I actually was in if I had not been awarded other grants. An
e-mail with the contract followed, which I filled and sent back by post and a
scanned copy via e-mail on Saturday. The deadline for the reception of the
signed contract was the end of Sunday. I'm now looking forward to the final
confirmation.

### The plan

I am going to spend 5 months working on my live visuals, which so far were
called *aBeLive*.

My existing live-set software is written in Kotlin and Processing. I worked on
it for 5 years and performed 13 times in Germany, Finland, UK, Spain, Taiwan and
online for Sweden. Over 10 gigs were cancelled during the Covid pandemic.

The software takes MIDI input from hardware devices I control during the
performance. It also takes live sound-analysis input.

For MIDI I use a 4-switch foot pedal plus two other MIDI controllers with 16
buttons and 16 rotary encoders each. In one of the controllers I use virtual
pages to access up to 64 inputs in total.

I do not use an external screen because I do not like the looks of someone
hidden behind a display on a performance. Since memorizing a high number of knobs
and buttons can be challenging, specially if I sometimes change their locations,
I wrote a web-based application to show the MIDI values on my mobile phone. I
try to memorize all inputs, but sometimes I need to look at the small screen to
verify the current values are what I expect. I find the small, almost invisible
display on the table better than having my face illuminated and partially
occluded by a laptop display.

## Monday, May 23rd 2022

I have received the signed contract by post. Things are moving forward.

### How has aBeLive evolved?

I didn't reach the current state in my visuals by taking a straight path. At
each stage I asked myself "what is the next step?". I am hoping that by starting
from scratch and having a better idea of where I want to get I can create a more
flexible system which produces more engaging visuals.

Several times I had to rewrite parts of the program to simplify the code.
Without doing that I felt unable to continue because I couldn't grasp the
situation. Before each performance I would push during days or weeks of intense
coding to implement new effects and behaviors.

Before the first London event in which Floating Spectrum performed without me
being present I developed an automation system trying to replace myself with a
program. The goal was to have interesting and evolving visuals during the whole
performance. I created the concept of "songs", and in each song the parameters
of the visuals would react in different ways to the music. This assumed the
songs would be performed in a specific order. It was scary to think that there
would be no one to react if things didn't look right. Apparently it worked
relatively well, although I'm sure it was not as captivating as if I was there
reacting to the sound and trying to tell my own story.

### Midi controllers

Midi controllers are used to modify the behavior of the visuals in real time. 
They frequently include buttons, knobs and / or sliders.

<img src="images/midi/midi_bcr2000.jpg" width="200">

The first controller I used was a **Behringer BCR-2000** with 32 knobs and ~28 buttons. At the beginning I performed just with this controller. Using only one controller made it easy to memorize what every knob and every button did. It allowed me to perform looking at the projection most of the time, rarely glancing at the controller. But it was too large for travel. It also required a power cable and a data cable, when other controllers are powered via the data cable. Therefore, I decided to try alternatives.

<img src="images/midi/midi_twister.jpg" width="200">

I replaced the BCR-2000 for a much more portable **Midi Fighter Twister** with 16 knobs, 22 buttons and 4 pages (for a total of 64 knobs and up to 70 buttons). Scaling down the number of knobs forced me to use the 4 virtual pages. This saved physical space, but worked against memorizing the location of inputs. Easier for travel but worse for performance.

<img src="images/webInterface.png" width="400">

To help me understand the purpose of each control I created a web page served from my computer and displayed in my **mobile phone**. This page shows the 16 values currently active in the Twister including names and matching the colors. The hues are not random but convey information about the purpose of each knob. It also shows what action is performed if the knob-button is pressed. That is an additional layer of complication: not only there are 4 pages, but knobs can be turned and also pressed.

<img src="images/midi/midi_faderfox.jpg"  width="200">

As the system grew and I needed more controls for post-processing effects I added a Faderfox EC4 controller, also with 16 knob-buttons, somewhat lighter and with a display to show names and current values. With this controller knobs can also perform a different action when pressed.

<img src="images/midi/midi_mkii.jpg"  width="200">

Switching between the 4 pages in the Twister by using the side buttons requires both hands. Therefore, I started using an **MK II** foot controller with 4 switches to change the page active in the twister (and shown in my phone).

After many hours of usage it has never become intuitive. I often end up trying pages 2, 3, 4... until I find the one I'm looking for by trial and error. This never happened in the BCR2000 because there were no pages. It is not good to have the same inputs mean different things depending on "state" (the current page). A 1-to-1 mapping is what really works and what I should use in the future.

I have invested much time in organizing the knobs and buttons into logical groups to make it more intuitive, but I am not yet satisfied, and I am considering what other ways are there to communicate with the computer during the performance.

![](images/midi/controllersComposition.svg)

In early stages I did feel like the "pilot of an organism". I could look around, get closer and farther, provoke shockwaves and sudden rotations. In later stages this has somewhat faded away. Some of my actions do change a larger-scale property, for example the locations of creatures around the main "actor", but this produces a slow change, a change in the destination, rather detached from my immediate actions.

I need to think further about this how to affect the visuals with my actions, and how to affect things short-term vs long-term.

<img src="images/midi/midi_keytar.jpg"  width="200">

In the next iteration I have decided to use a wireless key based controller. Keys are velocity sensitive which let me be more expressive than with previous controllers. The device tilt can be used as well to control parameters. I can move and feel my actions in my fingers and my body. I have enjoyed playing piano and synthesizers since I was a kid, so they feel natural to me. 

### Project goals

Due to how my performance tool was written, how it evolved and the framework I used to create it I have encountered limitations I want to overcome. Doing so requires rewriting my tool from scratch.

Originally the program was written using the Processing framework in IntelliJ Idea using the Java language. Before each performance I made improvements trying to make it more expressive and visually interesting.

During 2021 I converted most of the Java code to Kotlin. This made the code much shorter and easier to read and understand.

1. Presets, one per song.
2. Stick to one MIDI controller with1-to-1 mapping.
3. Be able to be more expressive. If I hear something happening, I should be able to express it with my actions, and it should be reflected on what is displayed.
4. More complexity. Don't just cotrol one creature, but an ecosystem.

## Thursday, May 26th

Today I merged the OPENRNDR code I have written during the last 2 ~ 3 years from my two computers. I will use parts of this code for the current project. I will also use older Processing code written during the last ~10 years, specially code from my existing live-set.

Most of the time was spent organizing code, mainly in two areas: getting the jpen software to work with OPENRNDR, and fixing the compute-shader programs I wrote during a workshop last year.

JPen is a library to access the pressure and tilt of a pen on a graphics tablet. I would like to be able to quickly draw creatures during my life performance, which is much simpler and more intuitive than coding a variety of shapes.

Compute Shaders are programs that run on the GPU. They are ideal for cases in which one needs hundreds of thousands or millions of entities interacting in real time (flocking behaviours, particles, smoke and fluids, etc). I plan to use CS to create organic looking visual effects.

## Friday 27th and Saturday, May 28th

I received an answer in the OPENRNDR Slack from Edwin. He found another graphic tablet library called [Stylus](https://github.com/lecturestudio/stylus). The great thing about it is that it is already packaged and exists in the mave repository, so I added it to my OPENRNDR project with one line of configuration in build.gradle.kts. I [asked](https://github.com/lectureStudio/stylus/issues/3) in their github repo about how to use the library and if it requires AWT or JAVAFX. I was too impatient to wait for an answer so I still tried to write a very simple OPENRNDR program that makes use of this library. The code did not have any syntax errors but when I try to run the program it crashes with a `SEVERE: Load stylus library failed` error so I guess I need to wait a bit.

## Monday, May 30th

Today I received the Keytar MIDI controller I ordered last week. I unpacked it, but I still haven't tried it.

I'm thinking I should convert this file from LibreOffice to markdown, so I can commit my changes every day. Then I can post links in [my Patreon page](https://www.patreon.com/funprogramming) and people could follow the progress.

- [x] Create the repo with this content in GitLab.
- [x] Add grant logo to the repository.
- [x] Create separate private repo (for now) for the code
- [x] Add OR basic template
- [x] Add 3D Camera and a rotating object

## Tuesday, May 31st

I tried to use the Stylus library and failed again, but I may have done some progress.
I was assuming that adding the dependency was enough, but I believe the dependency comes without the compiled C++ component that talks to the tablet, therefore I built the library using `mvn install` as shown in the README. First the build failed because my system had not set the `JAVA_HOME` environment variable. After fixing that the library did build, and it produced a `.so` library. I tried placing it in various folders but no luck. When I run my small OPENRNDR program which uses Stylus it fails to load the library. No answer yet from the authors.

To avoid Stylus blocking my progress I brought the Keytar controller and connected it to my computer. It comes with a wireless dongle which in theory is always linked to the controller (no need to configure anything). So next I'll try to visualize the data coming from the controller in OPENRNDR. 

- [x] Add MIDI listening
- [x] Figure out how to have a hud (items in ortho perspective not affected by the camera)

It does work. I can now rotate a 3D object based on the Keytar tilt. I also uses my Interpolator
class to not be limited to the low resolution of midi (128 steps) so it smoothly moves towards the desired target.

## June

### Wednesday 1st

Added `pitch bend` and `channel pressure` midi messages to the midi library. They were not implemented. 

### Thursday 2nd

Clean up the new `MidiVortex2.kt` midi helper. 

### Friday 3rd

Start implementing OSC listening based on previous aBeLive project.

### Monday 6th

Holiday

### Monday 13th

- Write in Ossia forum to ask if graphic tablets are supported in Ossia. 
I tried to reuse an old Processing pen library, but it didn't work. Then I
tried to use the Stylus library, but it also failed to run and the author
has not yet replied my question about how to make it work in Kotlin.
- Resume updating my compute shader code which I want to use in this project.
I started updating it on October 2021, but I never finished that. Today I
understood what's the current state and what needs to happen next.

### Tuesday 14th

- Ossia now supports graphic tablets! I tested and I could read position
and pressure correcly. I asked about an issue with tilt x and y being equal
even if they shouldn't.
- Continue finishing the compute-shader tweaks, so I can incorporate that in 
my visuals.

### 16th, 17th

Progress with compute-shaders

### 21st, 22nd, 23rd   
Progress with compute-shaders. Simplified SSBO for MovingAgents.

### 27th, 28th

Compute-shaders: simplify image/video exporting.

### 30th

Compute-shaders: make non-square windows work.

## July

### 4th

Compute-shaders: experiment with boids. Control their trails.
Try wrapping on edges instead of bouncing. Add frame rate indicator
to be aware of performance. Implement color variety.

### 5th

Compute-shaders: add interactions with mouse.

### 11th 

Compute-shaders: Add a set of random spawn points. Try black and white.

### 12th 

Add age and ageSpeed so boids can get old and die.
  
### 15th

Visit Ireland. Capture footage in nature to be used as input.

### 27th

Back in Berlin.

## August

### 1st

Resume development.
Figure out graphic tablet pressure and tilt input so I can draw during the 
performance.

### 7th

- [x] Add OSC listening based on previous aBeLive. Use existing playback tool,
      so I can hear sound and see their effect as simple graphics.
      Playback tool can be found at ~/workspace/aBeLive/playbackData2

### 8th

- [x] Complete MIDI, test all input types (key, slider, pad, slider, bend, tilt)
      experiment with the controller.
- [x] Load and play back video 

### 9th, 10th, 11th

- [x] Try figure out video current time
- [x] Figure out how to send video texture to compute shaders
      to control particles.
- [x] Create organic lines out of video

- [ ] Add videos recorded in Ireland.
- [ ] Add minimal compute shader to this project.
- [ ] Create physarum-based CS.
- [ ] Add post-processing

## September

### 23rd

- [x] Produce song analysis to visualize music

- [ ] Load song analysis and visualize it


## March 2023

### 21st

- [x] Upgrade template, fix deltaTime

Next: Play songs, play keytar at the same time, record data, observe data.


